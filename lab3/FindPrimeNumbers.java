public class FindPrimeNumbers {
	public static void main(String[] args) {

		int given = Integer.parseInt(args[0]);

		for (int number = 2; number < given; number++) {
			boolean isPrime = true;

			for (int divisior = 2; divisior < number; divisior++) {

				if (number % divisior == 0) {
					isPrime = false;

					break;
				}
			}
			if (isPrime)
				System.out.println(number+ ".");

		}
	}
}
