public class GCDLoop {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);

		System.out.println(GCD(a,b));
	}

	public static int GCD(int a, int b) {
		int r = 0;
		while(b != 0) {
			r = a % b;
			a = b;
			b = r;
		}
		return a;

	}
}
