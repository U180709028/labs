public class GCDRec {

	public static void main(String[] args) {
		int a = Integer.parseInt(args[0]);
		int b = Integer.parseInt(args[1]);
		System.out.println(GCD(a,b));
	}

	public static int GCD(int a, int b) {
		if (b == 0){
			return a;
		}else {
		return GCD(b, a % b);
		}
	}
}
