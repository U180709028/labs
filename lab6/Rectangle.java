public class Rectangle {
	int sideA;
	int sideB;
	Point topLeft;

	public Rectangle(Point p, int a, int b) {
		this.topLeft = p;
		this.sideA = a;
		this.sideB = b;
	}
	public int area() {
		return this.sideA * this.sideB;
	}
	public int perimeter() {
		return 2 *(this.sideA + this.sideB);
	}
	public Point[] corners() {
		Point[] parr = new Point[4];
		Point topRight = new Point((topLeft.xCoord + sideA) , topLeft.yCoord);
		Point bottomLeft = new Point(topLeft.xCoord , (topLeft.yCoord - sideB));
		Point bottomRight = new Point((topLeft.xCoord + sideA) , (topLeft.yCoord - sideB));
		parr[0] = this.topLeft;
		parr[1] = topRight;
		parr[2] = bottomLeft;
		parr[3] = bottomRight;
		return parr;
	}
}
